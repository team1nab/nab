# Nab

NAB Nail Bar, Lash Lounge &amp; Spa is dedicated to providing superior customized care through years of expertise, ongoing education, and extensive product knowledge. Our Skilled Nail, Lash &amp; Microblading specialists are highly trained with extensive training and years of experience working on acrylic nails with trending nail art designs, eyelash extensions from bold beautiful volume lashes with our Lash Artist certified in Lash Box Mega Volume Lashes.
https://www.nabnailbar.com/